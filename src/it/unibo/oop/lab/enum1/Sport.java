/**
 * 
 */
package it.unibo.oop.lab.enum1;

/**
 * Represents an enumeration for declaring sports;
 * 
 * 1) Complete the definition of the enumeration.
 * 
 */
public enum Sport {
    /*
     * declare the following sports: - basket - soccer - tennis - bike - F1 -
     * motogp - volley
     */
	
	Basket("Basket"),
	
	Soccer("Soccer"),
	
	Tennis("Tennis"),
	
	Bike("Bike"),
	
	F1("F1"),
	
	MotoGP("MotoGP"),
	
	Volley("Volley");
	
	
	
	private final String nameSport;
	
	private Sport(String nameSport) {
		this.nameSport = nameSport;
	}
	
	public String toString() {
		return this.nameSport;
	}
}
