package it.unibo.oop.lab.enum1;

import java.util.Comparator;

import it.unibo.oop.lab.socialnetwork.SocialNetworkUser;
import it.unibo.oop.lab.socialnetwork.User;

/**
 * This is going to act as a test for
 * {@link it.unibo.oop.lab.enum1.SportSocialNetworkUserImpl}
 * 
 * 1) Realize the same test as the previous exercise, this time using
 * enumeration Sport
 * 
 * 2) Run it: every test must return true.
 * 
 */
public final class TestSportByEnumeration {

    private TestSportByEnumeration() {
    }

    /**
     * @param args
     *            ignored
     */
     
    public static void main(final String... args) {
       
    	SportSocialNetworkUserImpl<User> mrossi = new SportSocialNetworkUserImpl<>("Mario", "Rossi", "mrossi", 52);	
    	SportSocialNetworkUserImpl<User> lverdi = new SportSocialNetworkUserImpl<>("Luigi", "Verdi", "lverdi", 42);	
    	SportSocialNetworkUserImpl<User> catezone = new SportSocialNetworkUserImpl<>("Caterina", "Amadei", "catezone", 20);	
    	
		/**
		*Add Rossi's Sport
		*/
    	
    	mrossi.addSport(Sport.Bike);
    	mrossi.addSport(Sport.Soccer);
    	mrossi.addSport(Sport.Volley);
   		
   		/**
		*Add Verdi's Sport
		*/
    	
    	mrossi.addSport(Sport.Tennis);
    	mrossi.addSport(Sport.Soccer);
    	mrossi.addSport(Sport.Volley);
    	/**
		*Add Cate's Sport
		*/
    	
    	mrossi.addSport(Sport.MotoGP);
    	mrossi.addSport(Sport.Soccer);
    	mrossi.addSport(Sport.Volley);
    	
   		
   		mrossi.addFollowedUser("Soccer Friend", lverdi);
   		mrossi.addFollowedUser("Volley Friend", catezone);
   		lverdi.addFollowedUser("Soccer Friend", mrossi);
   		catezone.addFollowedUser("None Friends", null);
   		
   		mrossi.getFollowedUsers().sort(new Comparator<User>(){

			public int compare(User o1, User o2) {
				return Integer.compare(o1.getAge(), o2.getAge());
			}
   		});
   		
   		System.out.println(mrossi.getFollowedUsers());
   		System.out.println();
   		System.out.println(lverdi.getFollowedUsers());
   		System.out.println();
   		System.out.println(catezone.getFollowedUsers());
   		}
   		

}
