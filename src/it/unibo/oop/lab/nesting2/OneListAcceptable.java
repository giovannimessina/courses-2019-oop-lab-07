package it.unibo.oop.lab.nesting2;
import java.util.*;
public class OneListAcceptable<T> implements Acceptable<T>{

	private List<T> list = null;
	
	public OneListAcceptable(final List<T> list){
		this.list = new ArrayList<>();
		this.list.addAll(list);
	
		
	}
	
	public Acceptor<T> acceptor() {
		Acceptor<T> acc = new AcceptorImpl<T>();
		return acc;
	
	}
	
	
	class AcceptorImpl<T> implements Acceptor<T>{

		private final static int FIRST_ELEMENT = 0;
		
		public void accept(T newElement) throws ElementNotAcceptedException {
			if(list.contains(newElement) && list.get(FIRST_ELEMENT) == newElement) {
				list.remove(FIRST_ELEMENT);
			}
			else {
				throw new ElementNotAcceptedException(newElement);
			}
			
		}

		public void end() throws EndNotAcceptedException {
			if(!list.isEmpty()) {
				throw new EndNotAcceptedException();
			}
			
		}


}
	
}
