# Instructions

Every exercise contains a `README.md` file with instructions on what to do.
Solve the exercise in the following order: 

1. `anonymous1` *V
2. `nesting1`   *V
3. `enum1` *V
4. `enum2` *V
5. `nesting2`*V
